
Output files are in json format, currently not suitable for use in anomaly detection with KNN-nearest neibghour method as algorithm behind PCA component analysis, there are some corelations with zeek's 'weird' log output, that prove some kind of anomaly existence (in this case staticstical corelation). Our logs for now need to be converted in zeek output log format explained at offical documentation site. And added a header with forwarded tags and expected data type, for faster reading. If not, error is raised. This script and method is taken from https://github.com/stratosphereips/zeek_anomaly_detector 


Principal component analysis (PCA) can be used in detecting outliers. 
PCA is a linear dimensionality reduction using Singular Value Decomposition of the data to project it to a lower dimensional space.
In this procedure, covariance matrix of the data can be decomposed to orthogonal vectors, called eigenvectors, associated with eigenvalues. 
The eigenvectors with high eigenvalues capture most of the variance in the data. Therefore, a low dimensional hyperplane constructed by k eigenvectors can capture most of the variance in the data. 
However, outliers are different from normal data points, which is more obvious on the hyperplane constructed by the eigenvectors with small eigenvalues. 
Therefore, outlier scores can be obtained as the sum of the projected distance of a sample on all eigenvectors. 
Score(X) = Sum of weighted euclidean distance between each sample to the hyperplane constructed by the selected eigenvectors.

Output example is shown in output.png


For now, most desirable logs are from conn.log zeek output file, as they monitor trafic flow. Basically there are two types of anomaly detection princliples, package and traffic monitoring.


 

